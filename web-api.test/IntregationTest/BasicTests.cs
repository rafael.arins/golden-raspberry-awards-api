using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using web_api.Models;
using Xunit;

namespace web_api.test.IntregationTest
{
    public class BasicTests : IClassFixture<CustomWebApplicationFactory<web_api.Startup>>
    {
        private readonly CustomWebApplicationFactory<web_api.Startup> _factory;

        public BasicTests(CustomWebApplicationFactory<web_api.Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("/api/movie")]
        [InlineData("/api/movie/GetByYear/1990")]
        [InlineData("/api/movie/YearsOneMoreWinners")]
        [InlineData("/api/producer/ProducerMaxMinInterval")]
        [InlineData("api/studio")]
        public async Task Get_EndpointsReturnSuccess(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("application/json; charset=utf-8", 
                response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task Get_and_Delete_Movie_EndpointsReturnSucces()
        {
             var client = _factory.CreateClient();

            // Act
            var get_response = await client.GetAsync("/api/movie");            
            
            var jsonFromPostResponse = await get_response.Content.ReadAsStringAsync();            
            var listMovie = JsonConvert.DeserializeObject<List<Movie>>(jsonFromPostResponse);
            var movie = listMovie.Where(m => !m.isWinner).FirstOrDefault();

            var delete_response = await client.DeleteAsync(string.Format("/api/movie/{0}", movie.Id)); 

            // Assert
            get_response.EnsureSuccessStatusCode();
            delete_response.EnsureSuccessStatusCode();
        }
    }
}