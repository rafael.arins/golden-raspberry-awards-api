namespace web_api.Helpers.CSV
{
    public class MovieCSVModel
    {
        public string year { get; set; }
        public string title { get; set; }
        public string studios { get; set; }
        public string producers { get; set; }
        public string winner { get; set; }
    }
}