using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using web_api.db.InMemory;
using Microsoft.EntityFrameworkCore;
using System;
using web_api.Models;

namespace web_api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly InMemoryContext _context;

        public MovieController(InMemoryContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all movies. (Producers and studios aren't load)
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/movie
        ///
        /// </remarks>
        /// <returns>A List of Movies</returns>
        /// <response code="200">Returns the list of movies or empty</response>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get()
        {
            var movies = await _context.Movies
                .Select(m => m).ToArrayAsync();

            return Ok(movies);
        }

        /// <summary>
        /// Get winners movies of a given year
        /// </summary>
        /// <param name="year">Year for search</param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/movie/GetByYear/1990
        ///
        /// </remarks>
        /// <returns>A List of Movies</returns>
        /// <response code="200">Returns the list of movies or empty</response>
        [HttpGet("[action]/{year}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetByYear(string year)
        {
            var movies = await _context.Movies
                .Where(m => m.isWinner && m.ReleaseDate.Year == int.Parse(year))
                .Include(m => m.MovieStudios)
                .Include(m => m.MovieProducers)
                .Select(m => new
                {
                    id = m.Id,
                    year = m.ReleaseDate.Year.ToString(),
                    title = m.Title,
                    studios = m.MovieStudios.Select(ms => ms.Studio.Name).ToArray(),
                    producers = m.MovieProducers.Select(mp => mp.Producer.Name).ToArray(),
                    winner = m.isWinner
                }).ToListAsync();

            return Ok(movies);
        }

        /// <summary>
        /// List of the years with more then one winner.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/movie/YearsOneMoreWinners
        ///
        /// </remarks>
        /// <returns>A List of years with winners count or empty</returns>
        /// <response code="200">Returns the list</response>
        [HttpGet("[action]")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> YearsOneMoreWinners()
        {
            var query = _context.Movies
                .Where(m => m.isWinner)
                .GroupBy(m => m.ReleaseDate, (key, group) => new
                {
                    year = key.Year,
                    winnerCount = group.Count()
                });

            var result = await query
                .Where(y => y.winnerCount > 1)
                .OrderByDescending(y => y.year)
                .OrderByDescending(y => y.winnerCount)
                .ToListAsync();

            return Ok(new { years = result });
        }

        /// <summary>
        /// Delete a movie.async Can't delete winners.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE api/movie/611b794f-76b2-4ed4-a2ac-6dc3d7932390
        ///
        /// </remarks>
        /// <returns>A string message</returns>
        /// <response code="200">Returns the success message</response>
        /// <response code="400">Returns the fail message</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var movie = await _context.Movies
                .Where(m => m.Id == id)
                .FirstOrDefaultAsync();

            if (movie.isWinner)
                return BadRequest(new { error = "Can't delete a winner movie" });

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return Ok(new { info = "Movie Deleted." });


        }
    }
}